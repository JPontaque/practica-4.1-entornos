package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class TestsClientes {
	
	private GestorContabilidad gestor;
	
	@Before
	public void setUp(){
		gestor = new GestorContabilidad();
		Cliente cliente = new Cliente("Jose", "1111A", LocalDate.now());
		Cliente cliente1 = new Cliente("Angel", "2222B", LocalDate.now());
		Cliente cliente2 = new Cliente("Nicolas", "3333C", LocalDate.now());
		gestor.altaCliente(cliente);
		gestor.altaCliente(cliente1);
		gestor.altaCliente(cliente2);
	}
	
	// Probar cliente con dni igual en la lista
	@Test
	public void testAltaClienteDniIgual(){
		Cliente cliente = new Cliente("JoseA", "1111A", LocalDate.now());
		gestor.altaCliente(cliente);
		
		assertEquals(3, gestor.getListaClientes().size());
		
	}
	
	//Probar cliente con dni distinto en la lista
	@Test
	public void testAltaClienteDniDistinto(){
		Cliente cliente = new Cliente("JoseA", "1111B", LocalDate.now());
		gestor.altaCliente(cliente);
		
		assertEquals(4, gestor.getListaClientes().size());
	}
	
	//Probar cliente con dni que esta en la lista
	@Test
	public void testBuscarClienteDniCorrecto(){
		Cliente cliente = gestor.buscarCliente("1111A");
		assertNotNull(cliente);
	}
	
	//Probar cliente con dni que no esta en la lista
	@Test
	public void testBuscarClienteDniFalso(){
		Cliente cliente = gestor.buscarCliente("AAAA1");
		assertNull(cliente);
	}
	
	//Probar cliente con mismo anno de fecha
	@Test
	public void testClienteMasAntiguoMismoAnno(){
		Cliente cliente = new Cliente("Fernando", "4444D", LocalDate.now());
		gestor.altaCliente(cliente);
		Cliente antiguo = gestor.clienteMasAntiguo();
		
		assertEquals(gestor.getListaClientes().get(0), antiguo);
	}
	
	//Probar eliminar cliente con dni que existe
	@Test
	public void testEliminarClienteExistente(){
		gestor.eliminarCliente("1111A");
		assertEquals(2, gestor.getListaClientes().size());
	}
	
	//Probar eliminar cliente con dni que no existe
	@Test
	public void testEliminarClienteInexistente(){
		gestor.eliminarCliente("1111B");
		assertEquals(3, gestor.getListaClientes().size());
	}
	
}