package clases;

import java.util.ArrayList;

public class GestorContabilidad {

	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;

	public GestorContabilidad() {
		listaFacturas = new ArrayList<>();
		listaClientes = new ArrayList<>();
	}

	public Cliente buscarCliente(String dni) {
		for (Cliente cliente : listaClientes) {
			if (cliente.getDni().equals(dni)) {
				return cliente;
			}
		}
		return null;
	}

	public Factura buscarFactura(String codigo) {
		for (Factura factura : listaFacturas) {
			if (factura.getCodigoFactura().equals(codigo)) {
				return factura;
			}
		}
		return null;
	}

	public void altaCliente(Cliente cliente) {
		int contador = 0;
		if (listaClientes.size() == 0) {
			listaClientes.add(cliente);
		} else {
			for (Cliente cliente1 : listaClientes) {
				if (cliente.getDni().equals(cliente1.getDni())) {
					contador++;
				}
			}
			if (contador == 0) {
				listaClientes.add(cliente);
			}
		}

	}

	public void crearFactura(Factura factura) {
		int contador = 0;
		if (listaFacturas != null) {
			for (Factura factura1 : listaFacturas) {
				if (factura.getCodigoFactura().equals(factura1.getCodigoFactura())) {
					contador++;
				}
			}
			if (contador == 0) {
				listaFacturas.add(factura);
			}
		}
	}

	public Cliente clienteMasAntiguo() {
		if (listaClientes != null) {
			Cliente clienteMasAntiguo = listaClientes.get(0);
			for (Cliente cliente : listaClientes) {
				if (clienteMasAntiguo.getFechaAlta().isAfter(cliente.getFechaAlta())) {
					clienteMasAntiguo = cliente;
				}
				return clienteMasAntiguo;
			}
		}
		return null;
	}

	public Factura facturaMasCara() {
		if (listaFacturas != null) {
			Factura facturaMasCara = listaFacturas.get(0);
			for (Factura factura : listaFacturas) {
				if (facturaMasCara.calcularPrecioFactura() < factura.calcularPrecioFactura()) {
					facturaMasCara = factura;
				}
				return facturaMasCara;
			}
		}
		return null;
	}

	public double calcularFacturacionAnual(int anno) {
		if (listaFacturas != null) {
			double precio = 0;
			for (Factura factura : listaFacturas) {
				if (factura.getFecha().getYear() == anno) {
					precio += factura.calcularPrecioFactura();
				}
			}
			return precio;
		}
		return 0;
	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {
		Cliente cliente = buscarCliente(dni);
		Factura factura = buscarFactura(codigoFactura);
		if (cliente != null && factura != null) {
			factura.setCliente(cliente);
		}
	}

	public int cantidadFacturasPorCliente(String dni) {
		Cliente cliente = buscarCliente(dni);
		int cantidad = 0;
		if (cliente != null) {
			for (Factura factura : listaFacturas) {
				if (factura.getCliente().getDni().equals(cliente.getDni())) {
					cantidad++;
				}
			}
			return cantidad;
		}
		return 0;
	}

	public void eliminarFactura(String codigo) {
		listaFacturas.remove(buscarFactura(codigo));
	}

	public void eliminarCliente(String dni) {
		listaClientes.remove(buscarCliente(dni));
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

}
