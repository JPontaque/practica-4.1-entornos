package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestsFacturas {

	private GestorContabilidad gestor;

	@Before
	public void setUp() {
		gestor = new GestorContabilidad();
		Cliente cliente = new Cliente("Jose", "1111A", LocalDate.now());
		Cliente cliente1 = new Cliente("Angel", "2222B", LocalDate.now());
		Cliente cliente2 = new Cliente("Nicolas", "3333C", LocalDate.now());
		gestor.altaCliente(cliente);
		gestor.altaCliente(cliente1);
		gestor.altaCliente(cliente2);
		Factura factura = new Factura("A1", LocalDate.now(), "Juego", 2, 2, cliente);
		Factura factura1 = new Factura("B2", LocalDate.now(), "Juego2", 3, 3, cliente1);
		Factura factura2 = new Factura("C3", LocalDate.now(), "Juego3", 4, 4, cliente2);
		gestor.crearFactura(factura);
		gestor.crearFactura(factura1);
		gestor.crearFactura(factura2);
	}

	// Probar que funciona si la factura es decimal
	@Test
	public void testCalcularPrecioFacturaPositivoDecimal() {
		Factura factura = new Factura();
		factura.setPrecioUnidad(2.5);
		factura.setCantidad(3);

		double actual = factura.calcularPrecioFactura();
		double esperado = 7.5;

		assertEquals(esperado, actual, 0.0);
	}

	// Probar que funciona se la factura es entera
	@Test
	public void testCalcularPrecioFacturaPositivoEntero() {
		Factura factura = new Factura();
		factura.setPrecioUnidad(2);
		factura.setCantidad(3);

		double actual = factura.calcularPrecioFactura();
		double esperado = 6;

		assertEquals(esperado, actual, 0.0);
	}

	// Probar factura con codigo distinto
	@Test
	public void testCrearFacturaCodigoDistinto() {
		Factura factura = new Factura("G5", LocalDate.now(), "Juego3", 4, 4, gestor.buscarCliente("1111A"));
		gestor.crearFactura(factura);
		assertEquals(4, gestor.getListaFacturas().size());
	}

	// Probar factura con codigo igual
	@Test
	public void testCrearFacturaCodigoIgual() {
		Factura factura = new Factura("A1", LocalDate.now(), "Juego3", 4, 4, gestor.buscarCliente("1111A"));
		gestor.crearFactura(factura);
		assertEquals(3, gestor.getListaFacturas().size());
	}

	// Probar factura con codigo que existe
	@Test
	public void testBuscarFacturaCodigoExistente() {
		Factura factura = gestor.buscarFactura("A1");
		assertNotNull(factura);
	}

	// Probar factura con codigo que no existe
	@Test
	public void testBuscarFacturaCodigoInexistente() {
		Factura factura = gestor.buscarFactura("AA");
		assertNull(factura);
	}

	// Probar factura mas cara
	@Test
	public void testFacturaMasCara() {
		Factura factura = gestor.facturaMasCara();
		assertEquals(16, factura.calcularPrecioFactura(), 0.0);
	}

	// Probar facturacion con anno existente en la lista
	@Test
	public void testFacturacionAnualExistente() {
		double factura = gestor.calcularFacturacionAnual(2018);
		assertEquals(29, factura, 0.0);
	}

	// Probar facturacion con anno no existente en la lista
	@Test
	public void testFacturacionAnualInexistente() {
		double factura = gestor.calcularFacturacionAnual(2017);
		assertEquals(0, factura, 0.0);
	}

	// Probar asignar un cliente a una factura existente
	@Test
	public void testAsignarClienteExistenteAFacturaExistente() {
		gestor.asignarClienteAFactura("1111A", "A1");
		Factura factura = gestor.buscarFactura("A1");
		assertNotNull(factura.getCliente());
	}

	// Probar asignar un cliente a una factura no existente
	@Test
	public void testAsignarClienteNoExistenteAFacturaNoExistente() {
		gestor.asignarClienteAFactura("1111A", "B1");
		Factura factura = gestor.buscarFactura("B1");
		assertEquals(null, factura);
	}

	// Probar cantidad de facturas con un cliente existente
	@Test
	public void testCantidadFacturasClienteExistente() {
		int cantidad = gestor.cantidadFacturasPorCliente("1111A");
		assertEquals(1, cantidad);
	}

	// Probar cantidad de facturas con un cliente no existente
	@Test
	public void testCantidadFacturasClienteInexistente() {
		int cantidad = gestor.cantidadFacturasPorCliente("3543A");
		assertEquals(0, cantidad);
	}

	// Probar eliminar factura con codigo que existe
	@Test
	public void testEliminarFacturaExistente() {
		gestor.eliminarFactura("A1");
		assertEquals(2, gestor.getListaFacturas().size());
	}

	// Probar eliminar factura con codigo que no existe
	@Test
	public void testEliminarFacturaInexistente() {
		gestor.eliminarFactura("AA");
		assertEquals(3, gestor.getListaFacturas().size());
	}

}
